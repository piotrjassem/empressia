## GIT 

GIT flow was used: https://docs.gitlab.com/ee/topics/gitlab_flow.html
It means there are main branches: develop, release and master. Features are made on 'feature' branches, the same go with bugfixes.

## Composer 
All related modules are included via composer

## Docker
This image was used:  'bitnami/symfony:1'

## Code

Entities, controllers and templates were generated via php bin/console generate:doctrine:crud

### Factory method
This design pattern was used for creating building for rent. So, there are classes: Flat, Hotels, etc. 
In this example code I used only object Flat, but it's prepared for modyfication.

### Builder
I assumed that the reservation is a large process. So, I used the Builder pattern for implementation. 
Please, check ReservationController.php -> 'new method', line 48
			$simpleReservation  = new SimpleReservationBuilder($entityManager);
			$simpleReservation
                ->setClient($client)
                ->setBeds($reservationData['bedNumber'])
                ->setBuilding($flat)
                ->setDates(new \DateTime($reservationData['startDate']), new \DateTime($reservationData['endDate']))
                ->countTotal()
                ->makeReservation($entityManager);
				
The idea is that this is a simple reservation, but if in future we'd like to a registration for f.e. "logged in" users, we add a different builder. 

### Data validation
I added date validation only in dates fields. It's made by Symfony\Component\Validator\Constraints as Assert.
If we'd like to use this, in "real life", we have to add validition and other fields (and of course in front).

### Tests
PHP Unit module is instaled. Two classes are added for tests, but at the current moment tests are not ready. 
Please, let me know if You'd like me to make them. 

				



