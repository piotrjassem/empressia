<?php


namespace App\FactoryMethod;


use App\Entity\Building;
use Doctrine\ORM\EntityManager;

/**
 * Interface Creator
 * @package App\FactoryMethod
 */
interface Creator
{
    /**
     * Creator constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager);

    /**
     * @return Building
     */
    public function createBuilding(): Building;
}
