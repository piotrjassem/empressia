<?php


namespace App\FactoryMethod;


use App\Entity\Building;
use App\Entity\Hotel;
use Doctrine\ORM\EntityManager;

class HotelCreator implements Creator
{

    public function createBuilding(): Building
    {
        return new Hotel();
    }

    public function __construct(EntityManager $entityManager)
    {
    }
}
