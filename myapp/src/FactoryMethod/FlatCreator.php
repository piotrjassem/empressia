<?php


namespace App\FactoryMethod;


use App\Entity\Building;
use App\Entity\Flat;
use Doctrine\ORM\EntityManager;

class FlatCreator implements Creator
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createBuilding(): Building
    {
        return $this->getDoctrine()->getRepository(Flat::class)->find(1);
    }
}
