<?php

namespace App\Entity;

use App\Repository\FlatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FlatRepository::class)
 */
class Flat implements Building
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Client::class, mappedBy="relation")
     */
    private $clients;

    /**
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="flat")
     */
    private $reservations;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;


    /**
     * @ORM\Column(type="integer")
     */
    private $beds;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * Flat constructor.
     */
    public function __construct()
    {
        $this->clients = new ArrayCollection();
        $this->reservations = new ArrayCollection();
    }




    /**
     * @return string
     */
    public function __toString() {
        return $this->getName();
    }


    /**
     * @param \DateTimeInterface $from
     * @param \DateTimeInterface $to
     * @param int $requestedBeds
     * @return bool
     */
    public function isAvailable( $from,  $to, int $requestedBeds)
    {
        $period = new \DatePeriod(
            new \DateTime($from),
            new \DateInterval('P1D'),
            new \DateTime($to)
        );
        foreach ($period as $key => $date) {
            $reservationPerDay = $this->getReservations()->filter(function (Reservation $reservation) use ($date){
               return $reservation->getStartDate() < $date && $reservation->getEndDate() > $date;
            });
            $sumInDay = 0;
            foreach ($reservationPerDay as $rpd){
                $sumInDay = $sumInDay + $rpd->getBedNumber();
            }

            if ($this->isNotEnoughBeds($sumInDay, $requestedBeds)){
                return false;
            }

        }
        return true;
    }

    /**
     * @return int|null
     */
    public function getBeds(): ?int
    {
        return $this->beds;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    /**
     * @param Client $client
     * @return $this
     */
    public function addClient(Client $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients[] = $client;
            $client->setRelation($this);
        }

        return $this;
    }

    /**
     * @param Client $client
     * @return $this
     */
    public function removeClient(Client $client): self
    {
        if ($this->clients->removeElement($client)) {
            // set the owning side to null (unless already changed)
            if ($client->getRelation() === $this) {
                $client->setRelation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    /**
     * @param Reservation $reservation
     * @return $this
     */
    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setFlat($this);
        }

        return $this;
    }

    /**
     * @param Reservation $reservation
     * @return $this
     */
    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getFlat() === $this) {
                $reservation->setFlat(null);
            }
        }

        return $this;
    }


    /**
     * @param int $beds
     * @return $this
     */
    public function setBeds(int $beds): self
    {
        $this->beds = $beds;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }
    /**
     * @param float $price
     * @return $this
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
    /**
     * @param int $sumInDay
     * @param int $requestedBeds
     * @return bool
     */
    private function isNotEnoughBeds(int $sumInDay, int $requestedBeds): bool
    {
        return $this->getBeds() < $sumInDay + $requestedBeds;
    }

}
