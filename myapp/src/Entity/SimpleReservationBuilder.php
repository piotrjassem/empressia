<?php


namespace App\Entity;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

/**
 * Class SimpleReservationBuilder
 * @package App\Entity
 */
class SimpleReservationBuilder implements ReservationBuilder
{
    /**
     *
     */
    const NUMBER_OF_DAYS_TO_GET_DISCOUNT = 7;
    const DISCOUNT = 0.9;
    /**
     * @var Reservation
     */
    protected $reservation;
    /**
     * @var EntityManager
     */
    private $entityManager;


    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->reservation = new Reservation();
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client): ReservationBuilder
    {
        $this->reservation->setClient($client);

        return $this;
    }


    /**
     * @param Building $building
     * @return $this|ReservationBuilder
     */
    public function setBuilding(Building $building): ReservationBuilder
    {
        $this->reservation->setFlat($building);

        return $this;
    }


    /**
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @return $this|ReservationBuilder
     */
    public function setDates(\DateTimeInterface $start, \DateTimeInterface $end): ReservationBuilder
    {
        $this->reservation->setStartDate($start);
        $this->reservation->setEndDate($end);

        return $this;
    }

    /**
     * @param int $beds
     */
    public function setBeds(int $beds): ReservationBuilder
    {
        $this->reservation->setBedNumber($beds);

        return $this;
    }

    /**
     *
     */
    public function countDiscount(): ReservationBuilder
    {
        if ($this->countDays() > self::NUMBER_OF_DAYS_TO_GET_DISCOUNT) {
            $this->reservation->setTotal($this->reservation->getTotal() * self::DISCOUNT);
        }

        return $this;
    }


    /**
     * @return Reservation
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function makeReservation(): Reservation
    {
        $this->entityManager->persist($this->reservation);
        $this->entityManager->flush();

        return $this->reservation;
    }

    /**
     * @return $this|ReservationBuilder
     * @throws \Exception
     */
    public function countTotal(): ReservationBuilder
    {
        $diff = $this->countDays();
        $total = $this->reservation->getBedNumber() * $diff * $this->reservation->getFlat()->getPrice();
        $this->reservation->setTotal($total);

        return $this;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function countDays(): string
    {
        $reservation = $this->reservation;

        return $reservation->getEndDate()->diff($reservation->getStartDate())->format("%a");
    }
}
