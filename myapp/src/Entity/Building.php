<?php


namespace App\Entity;


interface Building
{
    public function getBeds();

    public function isAvailable(\DateTimeInterface $from, \DateTimeInterface $to, int $beds);
}
