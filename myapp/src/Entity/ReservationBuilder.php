<?php


namespace App\Entity;


use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Doctrine\ORM\EntityManager;

interface ReservationBuilder
{
    public function __construct(EntityManager $entityManager);

    public function setClient(Client $client) : ReservationBuilder;

    public function setBuilding(Building $building): ReservationBuilder;

    public function setDates(\DateTimeInterface $start, \DateTimeInterface $end): ReservationBuilder;

    public function setBeds(int $beds): ReservationBuilder;

    public function countDiscount(): ReservationBuilder;

    public function countTotal(): ReservationBuilder;

    public function makeReservation() : Reservation;
}
