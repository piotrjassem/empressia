<?php


namespace App\Entity;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/**
 * Class ClientLoggedReservationBuilder
 * @package App\Entity
 */
class ClientLoggedReservationBuilder implements ReservationBuilder
{
    /**
     * @var Client
     */
    protected  $client;

    /**
     * ClientLoggedReservationBuilder constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
    }


    /**
     * @param Client $client
     * @return ReservationBuilder
     */
    public function setClient(Client $client) : ReservationBuilder
    {
    }


    /**
     *
     */
    public function countDiscount(): ReservationBuilder
    {
        // TODO: Implement countDiscount() method.
    }



    /**
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     */
    public function setDates($start, $end): ReservationBuilder
    {
        // TODO: Implement setDates() method.
    }

    /**
     * @param int $beds
     */
    public function setBeds(int $beds): ReservationBuilder
    {
        // TODO: Implement setBeds() method.
    }

    /**
     * @param Building $building
     */
    public function setBuilding(Building $building): ReservationBuilder
    {
        // TODO: Implement setBuilding() method.
    }

    /**
     * @return ReservationBuilder
     */
    public function countTotal(): ReservationBuilder
    {
        // TODO: Implement countTotal() method.
    }

    /**
     * @return Reservation
     */
    public function makeReservation(): Reservation
    {
        // TODO: Implement makeReservation() method.
    }


}
