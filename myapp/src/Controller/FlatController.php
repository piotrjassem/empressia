<?php

namespace App\Controller;

use App\Entity\Flat;
use App\Form\FlatType;
use App\Repository\FlatRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/flat")
 */
class FlatController extends AbstractController
{
    /**
     * @Route("/", name="flat_index", methods={"GET"})
     */
    public function index(FlatRepository $flatRepository): Response
    {
        return $this->render('flat/index.html.twig', [
            'flats' => $flatRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="flat_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $flat = new Flat();
        $form = $this->createForm(FlatType::class, $flat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($flat);
            $entityManager->flush();

            return $this->redirectToRoute('flat_index');
        }

        return $this->render('flat/new.html.twig', [
            'flat' => $flat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="flat_show", methods={"GET"})
     */
    public function show(Flat $flat): Response
    {
        return $this->render('flat/show.html.twig', [
            'flat' => $flat,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="flat_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Flat $flat): Response
    {
        $form = $this->createForm(FlatType::class, $flat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('flat_index');
        }

        return $this->render('flat/edit.html.twig', [
            'flat' => $flat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="flat_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Flat $flat): Response
    {
        if ($this->isCsrfTokenValid('delete'.$flat->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($flat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('flat_index');
    }
}
