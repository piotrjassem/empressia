<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Flat;
use App\Entity\Reservation;
use App\Entity\SimpleReservationBuilder;
use App\FactoryMethod\FlatCreator;
use App\Form\ReservationType;
use App\Repository\ReservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reservation")
 */
class ReservationController extends AbstractController
{
    /**
     * @Route("/", name="reservation_index", methods={"GET"})
     */
    public function index(ReservationRepository $reservationRepository): Response
    {
        return $this->render('reservation/index.html.twig', [
            'reservations' => $reservationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="reservation_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $reservation = new Reservation();
        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $reservationData = $request->request->get('reservation');
            $client = $this->prepareClient($reservationData['client']);
            $flat = $this->prepareFlat($reservationData);

            $simpleReservation  = new SimpleReservationBuilder($entityManager);
            $simpleReservation
                ->setClient($client)
                ->setBeds($reservationData['bedNumber'])
                ->setBuilding($flat)
                ->setDates(new \DateTime($reservationData['startDate']), new \DateTime($reservationData['endDate']))
                ->countTotal()
                ->makeReservation($entityManager);

            return $this->redirectToRoute('reservation_index');
        }

        return $this->render('reservation/new.html.twig', [
            'reservation' => $reservation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="reservation_show", methods={"GET"})
     */
    public function show(Reservation $reservation): Response
    {
        return $this->render('reservation/show.html.twig', [
            'reservation' => $reservation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="reservation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Reservation $reservation): Response
    {
        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reservation_index');
        }

        return $this->render('reservation/edit.html.twig', [
            'reservation' => $reservation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="reservation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Reservation $reservation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reservation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($reservation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('reservation_index');
    }

    /**
     * @param int $clientId
     * @return object
     */
    private function prepareClient(int $clientId): object
    {
        $client = $this->getDoctrine()->getRepository(Client::class)->find($clientId);
        if (!$client) {
            throw $this->createNotFoundException('No client found for id ' . $clientId);
        }
        return $client;
    }

    /**
     * @param array|null $reservationData
     * @return object|null
     */
    private function prepareFlat(?array $reservationData): object
    {
        $flat = $this->getDoctrine()->getRepository(Flat::class)->find($reservationData['flat']);
        if (!$flat->isAvailable($reservationData['startDate'], $reservationData['endDate'], $reservationData['bedNumber'])) {
            throw $this->createAccessDeniedException('Not available');
        }
        return $flat;
    }
}
